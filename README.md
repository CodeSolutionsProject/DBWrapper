# DBWrapper - Version 2.0.1

This script is a simple wrapper for SQLite3, MySQL and PgSQL, for make possible to use different BD systems without changing the functions.

## Installation

In composer:
```bash
composer require jkanetwork/dbwrapper "~2.0.1"
```

By hand: Download the php file inside src folder

## Avalible functions:
* To connect

    ```php
    //$server is location in SQLite3
    resource dbw_connect (string $tdb,string $server[,string $database, string $user,string $password]);
    ```

* To close

    ```php
     //Closes connection
    bool dbw_close (resource $conn);
    ```

* Queries
 
    ```php
    //Does a query (array, or false if error)
    resource dbw_query (resource $conn, string $query);
    //This does a (non interactive) multiquery. Its used for load from a file/script for example.
    bool dbw_multi_query(resource $conn, string $multiquery);
    //This do a query and fetch array (One restult), all in one function($typearray optional, see below)
    resource dbw_query_fetch_array (resource $conn,string $query[, string $typearray]);
    //Return query as array (All results), all in one function($typearray optional, see below)
    resource dbw_query_fetch_all (resource $conn,string $query[, string $typearray]);
    //dbw_fetch_all is too for queries)
    ```

* Using result of a query

    ```php
	//Fetch a row. ($typearray optional, see below)
    array dbw_fetch_array (resource $conn,resource $result[, string $typearray]);
    //Wrappers of dbw_fetch_array with row or assoc arguments
    array dbw_fetch_row(resource $conn,resource $result);
    array dbw_fetch_assoc(resource $conn,resource $result);
    //Goto X result of a query (Not retrieve it). If row is not specified, will be first row, 0
    bool dbw_query_goto(resource $conn,resource $result[,int $row]);
	// Wrappers to dbw_query_goto are dbw_data_seek and dbw_result_seek for compatibility
	
    //Return number of results of a query
    int dbw_num_rows(resource $conn, resource $result);
    ```

* Without a query

    ```php
    // Escapes conflictive chars for inserting into database
    string dbw_escape_string(resource $conn,string $string);
    //Returns last insert ID
    int dbw_last_id(resource $conn);
    //Alias of dbw_last_id
    int dbw_insert_id(resource $conn);
    ```

* Important details

    ```
    $tdb (Type of database) can be:
        -mysql/mysqli -> MySQL or MariaDB
        -sqlite/sqlite3 -> Sqlite3
        -PostgreSQL/PgSQL/pg -> PostgreSQL
        -mssql/sqlsrv -> Microsoft SQL Server (Needs sqlsrv lib installed)
        
    $conn is the connection stablished in dbw_connect (ie. $conn = dbw_connect('sqlite','file.sqlite'))
    
    $typearray is the form of array is returned, and not writed is default:
        -ASSOC -> Associative indexes
        -NUM -> Numeric indexes
        -BOTH -> (Default) Both types of indexes 
    ```

