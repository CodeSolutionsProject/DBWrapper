<?php
/*	DBWrapper.php - Version 2.01
	This script is a simple wrapper for SQLite3, MySQL, PgSQL and MSSQL (sqlsrv), 
	for make possible to use different BD systems without changing the functions.
	For use, in dbw_connect you have to specify type of database (see below)
	Read README.md for details of every function and use
*/


/** Connect with database */
function dbw_connect($tdb,$server,$database=NULL,$user = NULL,$password=NULL){ //Instance has to be \X,PORT for MSSQL
	$return = null;
	switch ($tdb){
		case "mysql":
		case "mysqli":
			$return[0] = mysqli_connect($server,$user,$password,$database) or throwExceptionDBConn();
			$return[1] = "mysqli"; //Return standard mysqli for other funcs.
		break;
		case "sqlite":
		case "sqlite3":
			$return[0] = new SQLite3($server);
			if (!$return[0]) throwExceptionDBConn();
			$return[1] = "sqlite"; //Return standard SQLite3 for other funcs.
		break;
		case "PostgreSQL":
		case "pg":
		case "PgSQL":
			$return[0] = pg_connect("host=$server dbname=$database user=$user password=$password") or throwExceptionDBConn();
			$return[1] = "PgSQL"; //Return standard PgSQL for other funcs.
		break;
		case "mssql": //MSSQL using sqlsrv
		case "SQLSRV": 
		case "sqlsrv":
			$serverName = "$server"; //If instance and port , has to be IP\INSTANCE:PORT
			$connectionOptions = array(
				"database" => "$database",
				"uid" => "$user",
				"pwd" => "$password"
			);
			$conn = sqlsrv_connect($serverName, $connectionOptions);
			if ($conn === false) {
				throwExceptionDBConn(var_dump(sqlsrv_errors()));
			}
			$return[0] = $conn;
			$return[1] = "sqlsrv";
		break;
		default:
			return false;
		break;
	}
	return $return;
}

function throwExceptionDBConn($str = "") {
	throw new Exception('Database connection error '.$str);
}

/** Escapes conflictive chars for inserting into database */
function dbw_escape_string($conn,$string){
	switch ($conn[1]){
		case "mysqli":
			return mysqli_escape_string($conn[0],$string);
		case "sqlite":
			return SQLite3::escapeString($string);
		case "PgSQL":
			return pg_escape_string($string);
		case "sqlsrv":
			return str_replace( "'", "''", $string ); //There is not a func for sqlsrv..
	}
}


/** Make query */
function dbw_query($conn,$query){
	$time = microtime(true);
	$ret = null;
	switch ($conn[1]){
		case "mysqli":
			$ret = mysqli_query($conn[0],$query);
		break;
		case "sqlite":
			$ret = $conn[0]->query($query);
		break;
		case "PgSQL":
			$ret = pg_query($query);
		break;
		case "sqlsrv":
			$ret = sqlsrv_query($conn[0],$query);
		break;

	}
	//echo "<p>".$query."->".(microtime(true)-$time)." milisegundos</p>";
	return $ret;
}

/** Fetch array from query (Next result) */
function dbw_fetch_array($conn,$result,$typearray = NULL){
	if ($result == false || $result == NULL){return false;}
	switch ($conn[1]){
		case "mysqli":
			if ($typearray == NULL || $typearray == "BOTH"){return mysqli_fetch_array($result,MYSQLI_BOTH);}
			if ($typearray == "ASSOC"){return mysqli_fetch_array($result,MYSQLI_ASSOC);}
			if ($typearray == "NUM"){return mysqli_fetch_array($result,MYSQLI_NUM);}
		break;
		case "sqlite":
			if ($typearray == NULL || $typearray == "BOTH"){return $result->fetchArray();}
			if ($typearray == "ASSOC"){return $result->fetchArray(SQLITE3_ASSOC);}
			if ($typearray == "NUM"){return $result->fetchArray(SQLITE3_NUM);}
		break;
		case "PgSQL":
			if ($typearray == NULL || $typearray == "BOTH"){return pg_fetch_array($result);}
			if ($typearray == "ASSOC"){return pg_fetch_array($result,NULL,PGSQL_ASSOC);}
			if ($typearray == "NUM"){return pg_fetch_array($result,NULL,PGSQL_NUM);}
		break;
		case "sqlsrv":
			if ($typearray == NULL || $typearray == "BOTH"){return sqlsrv_fetch_array($result,SQLSRV_FETCH_BOTH);}
			if ($typearray == "ASSOC"){return sqlsrv_fetch_array($result,NULL,SQLSRV_FETCH_ASSOC);}
			if ($typearray == "NUM"){return sqlsrv_fetch_array($result,NULL,SQLSRV_FETCH_NUMERIC);}
		break;
	}
}

/** Make query and fetch all in a array */
function dbw_fetch_all($conn,$result,$typearray = NULL){
	$time = microtime(true);
	$ret=null;
	switch ($conn[1]){
		case "mysqli":
			if ($typearray == NULL || $typearray == "BOTH"){$ret = mysqli_fetch_all($result,MYSQLI_BOTH);}
			if ($typearray == "ASSOC"){$ret = mysqli_fetch_all($result,MYSQLI_ASSOC);}
			if ($typearray == "NUM"){$ret = mysqli_fetch_all($result,MYSQLI_NUM);}
		break;
		case "sqlite":
			$ret = array();
			while ($row = $result->fetchArray()) { //There is not fetchAll in sqlite3, implement it
				$ret[] = ($row);
			}
		break;
		case "PgSQL":
			$ret = pg_fetch_all($result,PGSQL_ASSOC);
		break;
		case 'sqlsrv':
			$ret = array();
			if ($typearray == NULL || $typearray == "BOTH"){$a = SQLSRV_FETCH_BOTH;}
			if ($typearray == NULL || $typearray == "ASSOC"){$a = SQLSRV_FETCH_ASSOC;}
			if ($typearray == NULL || $typearray == "NUM"){$a = SQLSRV_FETCH_NUMERIC;}
			while ($row = sqlsrv_fetch_array($result,$a)) {
				$ret[] = ($row);
			}
		break;
	}
	//echo "<p>".$query."->".(microtime(true)-$time)." milisegundos</p>";
	return $ret;
}

/** Make query and fetch array (For one result) */
function dbw_query_fetch_array($conn,$query,$typearray = NULL){
	$time = microtime(true);
	$ret=null;
	$result = dbw_query($conn,$query);
	$ret = dbw_fetch_array($conn,$result,$typearray);
	//echo "<p>".$query."->".(microtime(true)-$time)." milisegundos</p>";
	return $ret;
}

/** Make query and fetch all in a array */
function dbw_query_fetch_all($conn,$query,$typearray = NULL){
	$time = microtime(true);
	$ret=null;
	$result = dbw_query($conn,$query);
	$ret = dbw_fetch_all($conn,$result,$typearray);
	//echo "<p>".$query."->".(microtime(true)-$time)." milisegundos</p>";
	return $ret;
}


/** Goes a query to $row. $row starts in 0 as first row as if not specified */
function dbw_query_goto($conn,$result,$row = 0){
	switch ($conn[1]){
		case "mysqli":
			mysqli_data_seek($result,$row);
		break;
		case "sqlite":
			$result->reset();
			$count = 0;
			while ($count != $row){
				$result->fetchArray();
			}
		break;
		case "PgSQL":
			pg_result_seek($result, $row);
		break;
		case "sqlsrv":
			sqlsrv_fetch($result,SQLSRV_SCROLL_ABSOLUTE,$row);
		break;	
	}
}

/** Does multiple querys in one command
 * The erasebuffer command, in mysqli is neccesary if its only a insert for avoid problems in next querys
*/
function dbw_multi_query($conn,$query,$erasebuffer = 0){
	switch ($conn[1]){
		case "mysqli":
			mysqli_multi_query($conn[0],$query);
			if ($erasebuffer){
				while(mysqli_next_result($conn[0])){;} //Erase multiquery output for avoid error in next query
			}
		break;
		case "sqlite":
			$conn[0]->exec($query);
		break;
		case "PgSQL":
			$null = pg_query($query);
		break;
		case "sqlsrv": //Returns TRUE/FALSE
			$ret = sqlsrv_prepare($conn[0],$query);
			$null = sqlsrv_execute($ret);
		break;
	}
}

/** Returns the lastest Insert ID */
function dbw_last_id($conn){
	switch ($conn[1]){
		case "mysqli":
			return mysqli_insert_id($conn[0]);
		case "sqlite":
			return $conn[0]->lastInsertRowID();
		case "PgSQL":
			return pg_fetch_array(pg_query("SELECT lastval();"))[0];
		case "sqlsrv":
			return die("Error, not implemented");
	}
}


/** Returns number of results */
function dbw_num_rows($conn,$result){
	switch ($conn[1]){
		case "mysqli":
			return mysqli_num_rows($result);
		case "sqlite":
		//Emulating num_rows. Is faster to use a COUNT(*), but for make it work...
			$count=0;
			$result->reset(); //Reset pointer
			while($row = $result->fetchArray(SQLITE3_ASSOC)){
				$count++;
			}
			$result->reset(); //Reset pointer
			return $count;
		case "PgSQL":
			return pg_num_rows ($result);
		case "sqlsrv":
			return sqlsrv_num_rows($result);

	}
}

/** Escapes conflictive chars for inserting into database */
function dbw_free_result($conn,$result){
	switch ($conn[1]){
		case "mysqli":
			return mysqli_free_result($string);
		case "sqlite":
			return NULL; //Not neccesary I think
		case "PgSQL":
			return NULL; //¿?
		case "sqlsrv":
			return sqlsrv_free_stmt($result);
	}
}


/** Close connection */
function dbw_close($conn){
	switch ($conn[1]){
		case "mysqli":
			mysqli_close($conn[0]);
		break;
		case "sqlite":
			$conn[0]->close();
		break;
		case "PgSQL":
			pg_close($conn[0]);
		break;
		case "sqlsrv":
			sqlsrv_close($conn[0]);
		break;
	}
}

/** Some internal wrappers for functions that are equal to other with arguments */

function dbw_fetch_assoc($conn,$result){return dbw_fetch_array($conn,$result,"ASSOC");}
function dbw_fetch_row($conn,$result){return dbw_fetch_array($conn,$result,"NUM");}
function dbw_query_fetch_one($conn,$query,$typearray = NULL){return dbw_query_fetch_array($conn,$query,$typearray);}

function dbw_data_seek($conn,$result){return dbw_query_goto($conn,$result,$row = 0);}
function dbw_result_seek($conn,$result){return dbw_query_goto($conn,$result,$row = 0);}

function dbw_insert_id($conn){return dbw_last_id($conn);}

?>
